# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui
from UiLogin import Ui_Login
from Dialog import Dialog
from ChatAPIBind import Chat
from Principal import MainWindow

class Login(QtGui.QMainWindow):
	def __init__(self):
		QtGui.QMainWindow.__init__(self)

		self.ui = Ui_Login() 
		self.ui.setupUi(self)
		self.connect(self.ui.pushButton, QtCore.SIGNAL('clicked()'), self.checkAndLogin)

	def checkAndLogin(self):
		if(self.ui.lineEdit_nick.text().isEmpty()):
			self.error = Dialog("Nick is empty", "Error");
		elif(not str(self.ui.lineEdit_cc.text()).isdigit()):
			self.error = Dialog("Country code must be numeric", "Error");
		elif(not str(self.ui.lineEdit_number.text()).isdigit()):
			self.error = Dialog("Number must be numeric", "Error");
		elif(not str(self.ui.lineEdit_pass.text()).isdigit() and not self.validMAC(str(self.ui.lineEdit_pass.text()))):
			self.error = Dialog("Invalid MAC or IMEI", "Error");
		else:
			c = Chat()
			if (c.connect()):
				if (c.login(str(self.ui.lineEdit_cc.text()+
							self.ui.lineEdit_number.text()),
							str(self.ui.lineEdit_pass.text()),
							str(self.ui.lineEdit_nick.text()))):
					self.hide();
					self.mw = MainWindow(self);
					self.mw.show();
				else:
					self.error = Dialog('Invalid user or password', 'Error')
			else:
				self.error = Dialog("Can't connect with Whatsapp", "Error")

	def validMAC(self, mac):
		return all((i < '0' or i > '9') and (i < 'A' or i > 'F') and (i != ':') for i in mac.upper())

	def closeEvent(self, event):
		Chat().close()
		event.accept()

if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	p = Login()
	p.show()
	sys.exit(app.exec_())
