# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Uilogin.ui'
#
# Created: Mon Dec 10 13:59:08 2012
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Login(object):
    def setupUi(self, Login):
        Login.setObjectName(_fromUtf8("Login"))
        Login.resize(272, 251)
        self.label_login = QtGui.QLabel(Login)
        self.label_login.setGeometry(QtCore.QRect(90, 10, 81, 31))
        font = QtGui.QFont()
        font.setPointSize(22)
        font.setBold(True)
        font.setItalic(True)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.label_login.setFont(font)
        self.label_login.setAlignment(QtCore.Qt.AlignCenter)
        self.label_login.setObjectName(_fromUtf8("label_login"))
        self.label = QtGui.QLabel(Login)
        self.label.setGeometry(QtCore.QRect(20, 60, 66, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Login)
        self.label_2.setGeometry(QtCore.QRect(20, 144, 61, 31))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(Login)
        self.label_3.setGeometry(QtCore.QRect(30, 164, 66, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(Login)
        self.label_4.setGeometry(QtCore.QRect(20, 170, 66, 31))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(Login)
        self.label_5.setGeometry(QtCore.QRect(20, 90, 91, 17))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_6 = QtGui.QLabel(Login)
        self.label_6.setGeometry(QtCore.QRect(20, 120, 66, 17))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.lineEdit_nick = QtGui.QLineEdit(Login)
        self.lineEdit_nick.setGeometry(QtCore.QRect(130, 50, 113, 27))
        self.lineEdit_nick.setObjectName(_fromUtf8("lineEdit_nick"))
        self.lineEdit_cc = QtGui.QLineEdit(Login)
        self.lineEdit_cc.setGeometry(QtCore.QRect(130, 80, 113, 27))
        self.lineEdit_cc.setObjectName(_fromUtf8("lineEdit_cc"))
        self.lineEdit_number = QtGui.QLineEdit(Login)
        self.lineEdit_number.setGeometry(QtCore.QRect(130, 120, 113, 27))
        self.lineEdit_number.setObjectName(_fromUtf8("lineEdit_number"))
        self.lineEdit_pass = QtGui.QLineEdit(Login)
        self.lineEdit_pass.setGeometry(QtCore.QRect(130, 160, 113, 27))
        self.lineEdit_pass.setEchoMode(QtGui.QLineEdit.Password)
        self.lineEdit_pass.setObjectName(_fromUtf8("lineEdit_pass"))
        self.pushButton = QtGui.QPushButton(Login)
        self.pushButton.setGeometry(QtCore.QRect(90, 210, 98, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))

        self.retranslateUi(Login)
        QtCore.QMetaObject.connectSlotsByName(Login)

    def retranslateUi(self, Login):
        Login.setWindowTitle(QtGui.QApplication.translate("Login", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.label_login.setText(QtGui.QApplication.translate("Login", "Login", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Login", "Nick", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Login", "IMEI", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Login", "or", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Login", "MAC", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Login", "Country Code", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("Login", "Number", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton.setText(QtGui.QApplication.translate("Login", "Go!", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton.setShortcut(QtGui.QApplication.translate("Login", "Return", None, QtGui.QApplication.UnicodeUTF8))

